
serve:
	python3 app.py --debug=True --port=8889

test:
	python3 -m unittest discover -s kiwiii.test

builddocs:
	cd docs && make html

chem:
	python3 ./scripts/sdf_demo.py

chemidx:
	python3 ./scripts/sdf_demo_index.py

assay:
	python3 ./scripts/text_demo.py
