---

sql: >
  CREATE TABLE NODATA (
    ID text primary key check(ID != '') collate nocase,
    Retest real,
    FP real,
    IC50 real,
    valid integer
  )

id: nodata
columns:
  - key: Retest
    name: __No Data__ Retest
    tags:
      - testAssay
      - test1
    valueType: inhibition%

  - key: IC50
    name: __No Data__ IC50
    tags:
      - testAssay
      - test1
    valueType: IC50


description: |
  emply data column test

---
ID	Retest	IC50
